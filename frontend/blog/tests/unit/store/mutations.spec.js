import mutations, { MUTATIONS } from "@/store/mutations";

const { ARTICLE, CATEGORY, ACCOUNT } = MUTATIONS;

describe("mutations.js", () => {
  describe("ARTICLE", () => {
    describe("FETCH_ALL_SUCCESS", () => {
      it('should set "articles"', () => {
        const state = {
          articles: []
        };
        mutations[ARTICLE.FETCH_ALL_SUCCESS](state, [{ id: 1 }, { id: 2 }]);
        expect(state.articles).toHaveLength(2);
      });
    });
    describe("FETCH_SUCCESS", () => {
      it('should set "article"', () => {
        const state = {
          article: []
        };
        mutations[ARTICLE.FETCH_SUCCESS](state, { id: 1 });
        expect(state.article.id).toBe(1);
      });
    });
    describe("CREATE_SUCCESS", () => {
      it("should add article", () => {
        const state = {
          articles: []
        };
        mutations[ARTICLE.CREATE_SUCCESS](state, { id: 1 });
        expect(state.articles).toHaveLength(1);
      });
    });
    describe("DELETE_SUCCESS", () => {
      it("should delete article", () => {
        const state = {
          articles: [{ id: 1 }]
        };
        mutations[ARTICLE.DELETE_SUCCESS](state, 1);
        expect(state.articles).toHaveLength(0);
      });
    });
  });
  describe("CATEGORY", () => {
    describe("FETCH_ALL_SUCCESS", () => {
      it('should set "categories"', () => {
        const state = {
          categories: []
        };
        mutations[CATEGORY.FETCH_ALL_SUCCESS](state, [{ id: 1 }, { id: 2 }]);
        expect(state.categories).toHaveLength(2);
      });
    });
    describe("FETCH_SUCCESS", () => {
      it('should set "category"', () => {
        const state = {
          category: []
        };
        mutations[CATEGORY.FETCH_SUCCESS](state, { id: 1 });
        expect(state.category.id).toBe(1);
      });
    });
  });
  describe("LOGIN_SUCCESS", () => {
    it('should set "loggedIn" to true', () => {
      const state = {
        loggedIn: false
      };
      mutations[ACCOUNT.LOGIN_SUCCESS](state, {});
      expect(state.loggedIn).toBe(true);
    });
  });
  describe("LOGOUT", () => {
    it('should set "loggedIn" to false', () => {
      const state = {
        loggedIn: true
      };
      mutations[ACCOUNT.LOGOUT](state);
      expect(state.loggedIn).toBe(false);
    });
  });
});
