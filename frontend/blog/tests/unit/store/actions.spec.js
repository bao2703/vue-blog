import sinon from "sinon";
import actions, { ACCOUNT } from "@/store/actions";
import { MUTATIONS } from "@/store/mutations";

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
  clear: jest.fn()
};
global.localStorage = localStorageMock;

describe("actions.js", () => {
  let commit;

  beforeEach(() => {
    commit = sinon.spy();
  });

  describe("LOGIN", () => {
    it("login", () => {
      const email = "test@email.com";
      const password = "password";

      actions[ACCOUNT.LOGIN]({ commit }, { email, password });

      //expect(commit.args).toEqual([[MUTATIONS.ACCOUNT.LOGIN_SUCCESS, {}]]);
    });
  });
  describe("CHECK_LOGIN", () => {
    it('should commit "LOGIN_SUCCESS"', () => {
      const user = { email: "test@email.com" };
      localStorage.setItem("user", JSON.stringify(user));

      actions[ACCOUNT.CHECK_LOGIN]({ commit });

      expect(commit.args).toEqual([[MUTATIONS.ACCOUNT.LOGIN_SUCCESS, user]]);
    });
    it('should not commit "LOGIN_SUCCESS"', () => {
      localStorage.removeItem("user");

      actions[ACCOUNT.CHECK_LOGIN]({ commit });

      expect(commit.args).toEqual([]);
    });
  });
});
