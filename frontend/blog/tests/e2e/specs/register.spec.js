describe("Register", () => {
  it("empty email", () => {
    cy.visit("/register");

    cy.get("input[type=email]").focus();

    cy.get("input[type=password]")
      .eq(0)
      .type("123");

    cy.get("input[type=password]")
      .eq(1)
      .type("123");

    cy.get("button[type=submit]").should("be.disabled");

    cy.get(".v-messages").should($e => {
      expect($e.eq(0)).to.contain("E-mail is required.");
    });
  });

  it("empty password", () => {
    cy.visit("/register");

    cy.get("input[type=email]").type("bao@gmail.com");

    cy.get("button[type=submit]")
      .click()
      .should("be.disabled");

    cy.get(".v-messages").should($e => {
      expect($e.eq(1)).to.contain("Password is required.");
      expect($e.eq(2)).to.contain("Confirm password is required.");
    });
  });

  it("confirm password not match", () => {
    cy.visit("/register");

    cy.get("input[type=email]").type("bao@gmail.com");

    cy.get("input[type=password]")
      .eq(0)
      .type("123");

    cy.get("input[type=password]")
      .eq(1)
      .type("1234");

    cy.get("button[type=submit]").should("be.disabled");

    cy.get(".v-messages").should($e => {
      expect($e.eq(2)).to.contain("Confirm password must match.");
    });
  });
});
