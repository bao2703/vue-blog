// https://docs.cypress.io/api/introduction/api.html

describe("Login", () => {
  afterEach(() => {
    cy.clearLocalStorage();
  });

  it("valid login", () => {
    cy.visit("/login");

    cy.get("input[type=email]").type("bao@gmail.com");

    cy.get("input[type=password]").type("123");

    cy.get("button[type=submit]").click();

    cy.url().should("eq", Cypress.config().baseUrl + "/");
  });

  it("wrong email", () => {
    cy.visit("/login");

    cy.get("input[type=email]").type("wrongemail@gmail.com");

    cy.get("input[type=password]").type("123");

    cy.get("button[type=submit]").click();

    cy.get(".v-alert").contains("Invalid login.");
  });

  it("wrong password", () => {
    cy.visit("/login");

    cy.get("input[type=email]").type("bao@gmail.com");

    cy.get("input[type=password]").type("wrongpassword");

    cy.get("button[type=submit]").click();

    cy.get(".v-alert").contains("Invalid login.");
  });

  it("empty email", () => {
    cy.visit("/login");

    cy.get("input[type=email]").focus();

    cy.get("input[type=password]").type("123");

    cy.get("button[type=submit]").should("be.disabled");

    cy.get(".v-messages").should($e => {
      expect($e.eq(0)).to.contain("E-mail is required.");
    });
  });

  it("empty password", () => {
    cy.visit("/login");

    cy.get("input[type=email]").type("bao@gmail.com");

    cy.get("button[type=submit]")
      .click()
      .should("be.disabled");

    cy.get(".v-messages").should($e => {
      expect($e.eq(1)).to.contain("Password is required.");
    });
  });

  it("empty email and password", () => {
    cy.visit("/login");

    cy.get("button[type=submit]")
      .click()
      .should("be.disabled");

    cy.get(".v-messages").should($e => {
      expect($e.eq(0)).to.contain("E-mail is required.");
      expect($e.eq(1)).to.contain("Password is required.");
    });
  });
});
