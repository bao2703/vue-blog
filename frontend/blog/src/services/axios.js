import axios from "axios";
import { BASE_URL } from "@/config";

const config = {
  baseURL: `${BASE_URL}/api`,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
};

const instance = axios.create(config);

instance.interceptors.request.use(config => {
  const user = JSON.parse(localStorage.getItem("user"));
  config.headers.Authorization = user ? `Bearer ${user.token}` : "";
  return new Promise(resolve => setTimeout(() => resolve(config), 1000));
  //return config;
});

export default instance;
