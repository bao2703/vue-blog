import axios from "./axios";

const resource = "account";

export default {
  getUserArticles() {
    return axios.get(`${resource}/articles`);
  },
  login(payload) {
    return axios.post(`${resource}/login`, payload);
  },
  register(payload) {
    return axios.post(`${resource}/register`, payload);
  }
};
