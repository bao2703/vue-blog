import axios from "./axios";

const resource = "article";

export default {
  getAll() {
    return axios.get(resource);
  },
  get(id) {
    return axios.get(`${resource}/${id}`);
  },
  create(payload) {
    return axios.post(resource, payload);
  },
  edit(payload) {
    return axios.put(resource, payload);
  },
  delete(id) {
    return axios.delete(`${resource}/${id}`);
  },
  upload(file) {
    let formData = new FormData();
    formData.append("file", file);
    return axios.post(`${resource}/upload`, formData, {
      headers: { "Content-Type": "multipart/form-data" }
    });
  }
};
