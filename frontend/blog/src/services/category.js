import axios from "./axios";

const resource = "category";

export default {
  getAll() {
    return axios.get(resource);
  },
  get(id) {
    return axios.get(`${resource}/${id}`);
  }
};
