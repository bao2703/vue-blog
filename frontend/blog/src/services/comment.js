import axios from "./axios";

const resource = "comment";

export default {
  create(payload) {
    return axios.post(resource, payload);
  }
};
