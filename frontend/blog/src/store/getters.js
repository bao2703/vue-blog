export default {
  filteredArticles: state => {
    const { articles, searchString } = state;
    return articles.filter(article =>
      article.title.toLowerCase().includes(searchString.toLowerCase())
    );
  }
};
