import router from "@/router";
import articleService from "@/services/article";
import categoryService from "@/services/category";
import commentService from "@/services/comment";
import accountService from "@/services/account";
import { MUTATIONS } from "./mutations";

export const ARTICLE = {
  FETCH_ALL: "[ARTICLE] FETCH_ALL",
  FETCH: "[ARTICLE] FETCH",
  CREATE: "[ARTICLE] CREATE",
  EDIT: "[ARTICLE] EDIT",
  DELETE: "[ARTICLE] DELETE",
  SEARCH: "[ARTICLE] SEARCH"
};

export const CATEGORY = {
  FETCH_ALL: "[CATEGORY] FETCH_ALL",
  FETCH: "[CATEGORY] FETCH"
};

export const COMMENT = {
  CREATE: "[COMMENT] CREATE"
};

export const ACCOUNT = {
  FETCH_ARTICLES: "[ACCOUNT] FETCH_ARTICLES",
  LOGIN: "[ACCOUNT] LOGIN",
  REGISTER: "[ACCOUNT] REGISTER",
  CHECK_LOGIN: "[ACCOUNT] CHECK_LOGIN",
  LOGOUT: "[ACCOUNT] LOGOUT"
};

export default {
  [ARTICLE.FETCH_ALL]({ commit }) {
    commit(MUTATIONS.TOGGLE_LOADING);
    return articleService.getAll().then(resp => {
      commit(MUTATIONS.ARTICLE.FETCH_ALL_SUCCESS, resp.data);
      commit(MUTATIONS.TOGGLE_LOADING);
    });
  },
  [ARTICLE.FETCH]({ commit }, id) {
    commit(MUTATIONS.TOGGLE_LOADING);
    return articleService.get(id).then(resp => {
      commit(MUTATIONS.ARTICLE.FETCH_SUCCESS, resp.data);
      commit(MUTATIONS.TOGGLE_LOADING);
    });
  },
  async [ARTICLE.CREATE]({ commit }, payload) {
    commit(MUTATIONS.TOGGLE_LOADING);
    const resp = await articleService.upload(payload.file);
    return articleService
      .create({ ...payload, picture: resp.data })
      .then(resp2 => {
        const article = resp2.data;
        router.push({ name: "article", params: { id: article.id } });
        commit(MUTATIONS.ARTICLE.CREATE_SUCCESS, article);
        commit(MUTATIONS.TOGGLE_LOADING);
      });
  },
  async [ARTICLE.EDIT]({ commit }, payload) {
    commit(MUTATIONS.TOGGLE_LOADING);
    const { file } = payload;
    payload.picture = "";
    if (file !== undefined && file !== "") {
      const resp = await articleService.upload(file);
      payload.picture = resp.data;
    }
    return articleService.edit(payload).then(resp => {
      commit(MUTATIONS.ARTICLE.EDIT_SUCCESS, resp.data);
      commit(MUTATIONS.TOGGLE_LOADING);
    });
  },
  [ARTICLE.DELETE]({ commit }, id) {
    return articleService.delete(id).then(() => {
      commit(MUTATIONS.ARTICLE.DELETE_SUCCESS, id);
    });
  },
  [ARTICLE.SEARCH]({ commit }, searchString) {
    commit(MUTATIONS.ARTICLE.SEARCH, searchString);
  },
  [CATEGORY.FETCH_ALL]({ commit }) {
    return categoryService.getAll().then(resp => {
      commit(MUTATIONS.CATEGORY.FETCH_ALL_SUCCESS, resp.data);
    });
  },
  [CATEGORY.FETCH]({ commit }, id) {
    commit(MUTATIONS.TOGGLE_LOADING);
    return categoryService.get(id).then(resp => {
      commit(MUTATIONS.CATEGORY.FETCH_SUCCESS, resp.data);
      commit(MUTATIONS.TOGGLE_LOADING);
    });
  },
  [COMMENT.CREATE]({ commit }, payload) {
    return commentService.create(payload).then(resp => {
      commit(MUTATIONS.COMMENT.CREATE_SUCCESS, resp.data);
    });
  },
  [ACCOUNT.FETCH_ARTICLES]({ commit }) {
    commit(MUTATIONS.TOGGLE_LOADING);
    return accountService.getUserArticles().then(resp => {
      commit(MUTATIONS.ARTICLE.FETCH_ALL_SUCCESS, resp.data);
      commit(MUTATIONS.TOGGLE_LOADING);
    });
  },
  [ACCOUNT.LOGIN]({ commit }, payload) {
    return accountService.login(payload).then(resp => {
      const user = resp.data;
      router.replace({ name: "home" });
      localStorage.setItem("user", JSON.stringify(user));
      commit(MUTATIONS.ACCOUNT.LOGIN_SUCCESS, user);
    });
  },
  [ACCOUNT.REGISTER]({ commit }, payload) {
    return accountService.register(payload).then(() => {
      router.push({ name: "login" });
      commit(MUTATIONS.ACCOUNT.REGISTER_SUCCESS);
    });
  },
  [ACCOUNT.CHECK_LOGIN]({ commit }) {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      commit(MUTATIONS.ACCOUNT.LOGIN_SUCCESS, user);
    }
  },
  [ACCOUNT.LOGOUT]({ commit }) {
    router.push({ name: "login" });
    localStorage.removeItem("user");
    commit(MUTATIONS.ACCOUNT.LOGOUT);
  }
};
