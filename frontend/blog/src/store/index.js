import Vue from "vue";
import Vuex from "vuex";

import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";

Vue.use(Vuex);

const state = {
  articles: [],
  categories: [],
  article: {},
  category: {},
  user: null,
  loggedIn: false,
  loading: false,
  searchString: ""
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  strict: process.env.NODE_ENV !== "production"
});
