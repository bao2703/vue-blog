const ARTICLE = {
  FETCH_ALL_SUCCESS: "[ARTICLE] FETCH_ALL_SUCCESS",
  FETCH_SUCCESS: "[ARTICLE] FETCH_SUCCESS",
  CREATE_SUCCESS: "[ARTICLE] CREATE_SUCCESS",
  EDIT_SUCCESS: "[ARTICLE] EDIT_SUCCESS",
  DELETE_SUCCESS: "[ARTICLE] DELETE_SUCCESS",
  SEARCH: "[ARTICLE] SEARCH"
};

const CATEGORY = {
  FETCH_ALL_SUCCESS: "[CATEGORY] FETCH_ALL_SUCCESS",
  FETCH_SUCCESS: "[CATEGORY] FETCH_SUCCESS"
};

const COMMENT = {
  CREATE_SUCCESS: "[COMMENT] CREATE_SUCCESS"
};

const ACCOUNT = {
  LOGIN_SUCCESS: "[ARTICLE] LOGIN_SUCCESS",
  REGISTER_SUCCESS: "[ARTICLE] REGISTER_SUCCESS",
  LOGOUT: "[ARTICLE] LOGOUT"
};

const TOGGLE_LOADING = "TOGGLE_LOADING";

export default {
  [ARTICLE.FETCH_ALL_SUCCESS](state, payload) {
    state.articles = payload;
  },
  [ARTICLE.FETCH_SUCCESS](state, payload) {
    state.article = payload;
  },
  [ARTICLE.CREATE_SUCCESS](state, payload) {
    state.articles.unshift(payload);
  },
  [ARTICLE.EDIT_SUCCESS](state, payload) {
    let newArticles = [...state.articles];
    const index = newArticles.findIndex(a => a.id === payload.id);
    newArticles[index] = { ...payload, user: state.user };
    state.articles = newArticles;
  },
  [ARTICLE.DELETE_SUCCESS](state, id) {
    state.articles = state.articles.filter(a => a.id !== id);
  },
  [ARTICLE.SEARCH](state, searchString) {
    state.searchString = searchString;
  },
  [CATEGORY.FETCH_ALL_SUCCESS](state, payload) {
    state.categories = payload;
  },
  [CATEGORY.FETCH_SUCCESS](state, payload) {
    state.category = payload;
  },
  [COMMENT.CREATE_SUCCESS](state, payload) {
    state.article.comments.unshift(payload);
  },
  [ACCOUNT.LOGIN_SUCCESS](state, payload) {
    state.loggedIn = true;
    state.user = payload;
  },
  [ACCOUNT.REGISTER_SUCCESS]() {},
  [ACCOUNT.LOGOUT](state) {
    state.loggedIn = false;
    state.user = null;
  },
  [TOGGLE_LOADING](state) {
    state.loading = !state.loading;
  }
};

export const MUTATIONS = {
  ARTICLE,
  CATEGORY,
  COMMENT,
  ACCOUNT,
  TOGGLE_LOADING
};
