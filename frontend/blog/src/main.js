import Vue from "vue";
import "./plugins/vuetify";
import "./plugins/froala-wysiwyg";
import "./registerServiceWorker";

import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import { ACCOUNT } from "./store/actions";
import { BASE_URL } from "@/config";

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    store.dispatch(ACCOUNT.CHECK_LOGIN).then(() => {
      if (store.state.loggedIn) {
        next();
      } else {
        next({ name: "login" });
      }
    });
  } else {
    next();
  }
});

Vue.config.productionTip = false;

Vue.mixin({
  data: function() {
    return {
      get BASE_URL() {
        return BASE_URL;
      }
    };
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
