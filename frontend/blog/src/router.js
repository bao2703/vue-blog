import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const CreateArticle = () => import("./views/CreateArticle.vue");
const Register = () => import("./views/Register.vue");
const Login = () => import("./views/Login.vue");
const Category = () => import("./views/Category.vue");
const Article = () => import("./views/Article.vue");
const Profile = () => import("./views/Profile.vue");

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/create",
      name: "create",
      component: CreateArticle,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/register",
      name: "register",
      component: Register
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/category/:id",
      name: "category",
      props: true,
      component: Category
    },
    {
      path: "/article/:id",
      name: "article",
      props: true,
      component: Article
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
      meta: {
        requiresAuth: true
      }
    },
    // otherwise redirect to home
    { path: "*", redirect: "/" }
  ]
});
