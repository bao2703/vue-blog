﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using Blog.Controllers;
using Blog.Data;
using Blog.Dtos;
using Blog.Repositories;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace BlogTests.Controllers
{
    public class ArticlesControllerTests
    {
        private readonly List<Article> _data = new List<Article>
        {
            new Article {Id = 1, Title = "Test 1", Content = "Test 1", UserId = 1, Picture = "test1.jpg" },
            new Article {Id = 2, Title = "Test 2", Content = "Test 2"},
            new Article {Id = 3, Title = "Test 3", Content = "Test 3"},
            new Article {Id = 4, Title = "Test 4", Content = "Test 4"},
            new Article {Id = 5, Title = "Test 5", Content = "Test 5"}
        };

        [Fact]
        public void Create_ModelStateNotValid_ReturnsBadRequest()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            var controller = new ArticleController(null, mockArticleRepo.Object, null, null);
            controller.ModelState.AddModelError(string.Empty, "Error");

            // Act
            var result = controller.Create(null);

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void Create_ShouldCallAdd_ReturnsOk()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockArticleRepo = new Mock<IArticleRepository>();
            var mockUserRepo = new Mock<IUserRepository>();

            var controller =
                new ArticleController(mockUnitOfWork.Object, mockArticleRepo.Object, mockUserRepo.Object, null);

            // Act
            var result = controller.Create(new ArticleDto());

            // Assert
            result.Should().BeOfType<OkObjectResult>();

            mockArticleRepo.Verify(r => r.Add(It.IsAny<Article>()), Times.Once);
            mockUnitOfWork.Verify(r => r.SaveChanges(), Times.Once);
        }

        [Fact]
        public void Delete_InValidOwner_ReturnsNoContent()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            mockArticleRepo
                .Setup(r => r.Find(It.IsAny<int>()))
                .Returns(_data[0]);

            const int userId = 2;
            var user = new ClaimsPrincipal(new ClaimsIdentity(new[] {new Claim("id", userId.ToString())}));
            var controller = new ArticleController(null, mockArticleRepo.Object, null, null)
            {
                ControllerContext = new ControllerContext {HttpContext = new DefaultHttpContext {User = user}}
            };

            // Act
            var result = controller.Delete(It.IsAny<int>());

            // Assert
            result.Should().BeOfType<BadRequestResult>();

            mockArticleRepo.Verify(r => r.Remove(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public void Delete_NotFound_ReturnsBadRequest()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            var controller = new ArticleController(null, mockArticleRepo.Object, null, null);

            // Act
            var result = controller.Delete(It.IsAny<int>());

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void Delete_ValidOwner_ShouldCallDelete_ReturnsNoContent()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockArticleRepo = new Mock<IArticleRepository>();
            var mockEnv = new Mock<IHostingEnvironment>();

            mockEnv.Setup(s => s.WebRootPath).Returns("wwwroot");

            mockArticleRepo
                .Setup(r => r.Find(It.IsAny<int>()))
                .Returns(_data[0]);

            const int userId = 1;
            var user = new ClaimsPrincipal(new ClaimsIdentity(new[] {new Claim("id", userId.ToString())}));
            var controller = new ArticleController(mockUnitOfWork.Object, mockArticleRepo.Object, null, mockEnv.Object)
            {
                ControllerContext = new ControllerContext {HttpContext = new DefaultHttpContext {User = user}}
            };

            // Act
            var result = controller.Delete(It.IsAny<int>());

            // Assert
            result.Should().BeOfType<NoContentResult>();

            mockArticleRepo.Verify(r => r.Remove(It.IsAny<Article>()), Times.Once);
            mockUnitOfWork.Verify(r => r.SaveChanges(), Times.Once);
        }

        [Fact]
        public void Edit_ModelStateNotValid_ReturnsBadRequest()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            var controller = new ArticleController(null, mockArticleRepo.Object, null, null);
            controller.ModelState.AddModelError(string.Empty, "Error");

            // Act
            var result = controller.Edit(null);

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void Edit_NotFound_ReturnsBadRequest()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            var controller = new ArticleController(null, mockArticleRepo.Object, null, null);

            // Act
            var result = controller.Edit(new ArticleDto { Id = It.IsAny<int>() });

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void Edit_ShouldCallUpdate_ReturnsOk()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockArticleRepo = new Mock<IArticleRepository>();
            var mockEnv = new Mock<IHostingEnvironment>();

            mockEnv.Setup(s => s.WebRootPath).Returns("wwwroot");

            mockArticleRepo.Setup(r => r.Find(It.IsAny<int>())).Returns(_data[0]);

            var controller = new ArticleController(mockUnitOfWork.Object, mockArticleRepo.Object, null, mockEnv.Object);

            // Act
            var result = controller.Edit(new ArticleDto { Id = It.IsAny<int>(), Picture = "upload/test.jpg" });

            // Assert
            result.Should().BeOfType<OkObjectResult>();

            mockArticleRepo.Verify(r => r.Update(It.IsAny<Article>()), Times.Once);
            mockUnitOfWork.Verify(r => r.SaveChanges(), Times.Once);
        }

        [Fact]
        public void Get_Found_ReturnsOk()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            mockArticleRepo
                .Setup(s => s.Include(It.IsAny<Expression<Func<Article, object>>[]>()))
                .Returns(_data);

            var controller = new ArticleController(null, mockArticleRepo.Object, null, null);

            // Act
            var result = controller.Get(1);

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var article = okResult.Value.Should().BeAssignableTo<Article>().Subject;
            article.Id.Should().Be(1);
        }

        [Fact]
        public void Get_NotFound_ReturnsBadRequest()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            var controller = new ArticleController(null, mockArticleRepo.Object, null, null);

            // Act
            var result = controller.Get(It.IsAny<int>());

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void GetAll_ReturnsOk()
        {
            // Arrange
            var mockArticleRepo = new Mock<IArticleRepository>();

            mockArticleRepo
                .Setup(s => s.Include(It.IsAny<Expression<Func<Article, object>>[]>()))
                .Returns(_data);

            var controller = new ArticleController(null, mockArticleRepo.Object, null, null);

            // Act
            var result = controller.Get();

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var articles = okResult.Value.Should().BeAssignableTo<IEnumerable<Article>>().Subject;
            articles.Count().Should().Be(5);
        }

        [Fact]
        public void Upload_InvalidFormat_ReturnsBadRequest()
        {
            // Arrange
            var fileMock = new Mock<IFormFile>();
            var memoryStream = new MemoryStream();
            var writer = new StreamWriter(memoryStream);

            writer.Write("Fake content");
            writer.Flush();
            memoryStream.Position = 0;

            fileMock.Setup(r => r.OpenReadStream()).Returns(memoryStream);
            fileMock.Setup(r => r.FileName).Returns("test.exe");
            fileMock.Setup(r => r.Length).Returns(memoryStream.Length);

            var controller = new ArticleController(null, null, null, null);

            // Act
            var result = controller.Upload(fileMock.Object);

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }
    }
}