﻿using Blog.Controllers;
using Blog.Data;
using Blog.Dtos;
using Blog.Repositories;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace BlogTests.Controllers
{
    public class CommentControllerTests
    {
        [Fact]
        public void Create_ModelStateNotValid_ReturnsBadRequest()
        {
            // Arrange
            var mockCommentRepo = new Mock<ICommentRepository>();

            var controller = new CommentController(null, mockCommentRepo.Object, null);
            controller.ModelState.AddModelError(string.Empty, "Error");

            // Act
            var result = controller.Create(null);

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void Create_ShouldCallAdd_ReturnsOk()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockCommentRepo = new Mock<ICommentRepository>();
            var mockUserRepo = new Mock<IUserRepository>();

            var controller = new CommentController(mockUnitOfWork.Object, mockCommentRepo.Object, mockUserRepo.Object);

            // Act
            var result = controller.Create(new CommentDto());

            // Assert
            result.Should().BeOfType<OkObjectResult>();

            mockCommentRepo.Verify(r => r.Add(It.IsAny<Comment>()), Times.Once);
            mockUnitOfWork.Verify(r => r.SaveChanges(), Times.Once);
        }
    }
}