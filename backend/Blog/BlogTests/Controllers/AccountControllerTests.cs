﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Blog.Common;
using Blog.Controllers;
using Blog.Data;
using Blog.Dtos;
using Blog.Repositories;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace BlogTests.Controllers
{
    public class AccountControllerTests
    {
        [Fact]
        public void Login_InValidUser()
        {
            // Arrange
            var mockUserRepo = new Mock<IUserRepository>();

            var controller = new AccountController(null, mockUserRepo.Object);

            // Act
            var result = controller.Login(new UserDto());

            // Assert
            var badRequestResult = result.Should().BeOfType<BadRequestObjectResult>().Subject;
            badRequestResult.Value.Should().BeOfType<ErrorResponse>();
        }

        [Fact]
        public void Login_ModelStateNotValid_ReturnsBadRequest()
        {
            // Arrange
            var controller = new AccountController(null, null);
            controller.ModelState.AddModelError(string.Empty, "Error");

            // Act
            var result = controller.Login(new UserDto());

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void Login_ValidUser_ReturnsToken()
        {
            // Arrange
            var mockUserRepo = new Mock<IUserRepository>();
            mockUserRepo
                .Setup(s => s.Authenticate(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new User { Id = 1, Email = "test@gmail.com" });
            mockUserRepo
                .Setup(s => s.GenerateJwt(It.IsAny<User>()))
                .Returns("Token");

            var controller = new AccountController(null, mockUserRepo.Object);

            // Act
            var result = controller.Login(new UserDto());

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var user = okResult.Value.Should().BeOfType<UserDto>().Subject;
            user.Email.Should().Be("test@gmail.com");
            user.Token.Should().Be("Token");
        }

        [Fact]
        public void Register_AvailableEmail_ReturnsOk()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockUserRepo = new Mock<IUserRepository>();

            mockUserRepo
                .Setup(s => s.IsAvailableEmail(It.IsAny<string>()))
                .Returns(true);

            var controller = new AccountController(mockUnitOfWork.Object, mockUserRepo.Object);

            // Act
            var result = controller.Register(new UserDto());

            // Assert
            result.Should().BeOfType<OkResult>();
        }

        [Fact]
        public void Register_ModelStateNotValid_ReturnsBadRequest()
        {
            // Arrange
            var controller = new AccountController(null, null);
            controller.ModelState.AddModelError(string.Empty, "Error");

            // Act
            var result = controller.Register(new UserDto());

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void Register_NotAvailableEmail_ReturnsBadRequest()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockUserRepo = new Mock<IUserRepository>();

            mockUserRepo.Setup(s => s.IsAvailableEmail(It.IsAny<string>())).Returns(false);

            var controller = new AccountController(mockUnitOfWork.Object, mockUserRepo.Object);

            // Act
            var result = controller.Register(new UserDto());

            // Assert
            var badRequestResult = result.Should().BeOfType<BadRequestObjectResult>().Subject;
            badRequestResult.Value.Should().BeOfType<ErrorResponse>();
        }

        [Fact]
        public void GetUserArticles_ReturnsOk()
        {
            // Arrange
            var data = new List<Article>
            {
                new Article {Id = 1, Title = "Test 1", Content = "Test 1", UserId = 1},
                new Article {Id = 2, Title = "Test 2", Content = "Test 2", UserId = 1}
            };

            var mockUserRepo = new Mock<IUserRepository>();

            mockUserRepo
                .Setup(s => s.GetUserArticles(It.IsAny<int>()))
                .Returns(data);

            var userId = It.IsAny<int>();
            var user = new ClaimsPrincipal(new ClaimsIdentity(new[] { new Claim("id", userId.ToString()) }));
            var controller = new AccountController(null, mockUserRepo.Object)
            {
                ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext { User = user } }
            };

            // Act
            var result = controller.GetUserArticles();

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var articles = okResult.Value.Should().BeAssignableTo<IEnumerable<Article>>().Subject;
            articles.Count().Should().Be(2);
        }
    }
}