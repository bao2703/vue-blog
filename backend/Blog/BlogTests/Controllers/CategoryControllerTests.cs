﻿using System.Collections.Generic;
using System.Linq;
using Blog.Controllers;
using Blog.Data;
using Blog.Repositories;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace BlogTests.Controllers
{
    public class CategoryControllerTests
    {
        private readonly List<Category> _data = new List<Category>
        {
            new Category {Id = 1, Name = "Test 1"},
            new Category {Id = 2, Name = "Test 2"},
            new Category {Id = 3, Name = "Test 3"},
            new Category {Id = 4, Name = "Test 4"},
            new Category {Id = 5, Name = "Test 5"}
        };

        [Fact]
        public void Get_Found_ReturnsOk()
        {
            // Arrange
            var mockCategoryRepo = new Mock<ICategoryRepository>();

            mockCategoryRepo
                .Setup(s => s.Include(It.IsAny<string[]>()))
                .Returns(_data);

            var controller = new CategoryController(null, mockCategoryRepo.Object);

            // Act
            var result = controller.Get(1);

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var category = okResult.Value.Should().BeAssignableTo<Category>().Subject;
            category.Id.Should().Be(1);
        }

        [Fact]
        public void Get_NotFound_ReturnsBadRequest()
        {
            // Arrange
            var mockCategoryRepo = new Mock<ICategoryRepository>();

            var controller = new CategoryController(null, mockCategoryRepo.Object);

            // Act
            var result = controller.Get(It.IsAny<int>());

            // Assert
            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public void GetAll_ReturnsOk()
        {
            // Arrange
            var mockCategoryRepo = new Mock<ICategoryRepository>();

            mockCategoryRepo
                .Setup(s => s.GetAll())
                .Returns(_data);

            var controller = new CategoryController(null, mockCategoryRepo.Object);

            // Act
            var result = controller.Get();

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var categories = okResult.Value.Should().BeAssignableTo<IEnumerable<Category>>().Subject;
            categories.Count().Should().Be(5);
        }
    }
}