﻿using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Blog.Common
{
    public static class ImageHelper
    {
        private const int MaxWidth = 600;

        public static Image<Rgba32> Resize(Stream stream)
        {
            var image = Image.Load(stream);

            if (image.Width <= MaxWidth) return image;

            var ratio = image.Height / image.Width;
            image.Mutate(x => x.Resize(MaxWidth, MaxWidth * ratio));

            return image;
        }

        public static bool IsValid(IFormFile file)
        {
            const int maxSize = 10;
            var validExtensions = new[] {".jpg", ".jpeg", ".png", ".gif"};
            return validExtensions.Contains(Path.GetExtension(file.FileName)) && file.Length < maxSize * 1024 * 1024;
        }
    }
}