﻿using System;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Blog.Common
{
    public class JwtSettings
    {
        public static string Secret = "Very long long secret";

        public static SigningCredentials SigningCredentials =
            new SigningCredentials(SymmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

        public static DateTime Expires => DateTime.Now.AddDays(1);

        public static SymmetricSecurityKey SymmetricSecurityKey =>
            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret));
    }
}