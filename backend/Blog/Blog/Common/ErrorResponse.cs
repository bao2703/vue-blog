﻿namespace Blog.Common
{
    public class ErrorResponse
    {
        public static ErrorResponse InvalidLogin = new ErrorResponse(1, "Invalid login.");
        public static ErrorResponse NotAvailableEmail = new ErrorResponse(2, "Not available email.");

        public ErrorResponse(int code, string message)
        {
            Code = code;
            Message = message;
        }

        public int Code { get; set; }

        public string Message { get; set; }
    }
}