﻿using System;

namespace Blog.Dtos
{
    public class ArticleDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Picture { get; set; }

        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public int CategoryId { get; set; }

        public int UserId { get; set; }
    }
}