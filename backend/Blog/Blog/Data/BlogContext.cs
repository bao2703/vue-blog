﻿using Microsoft.EntityFrameworkCore;

namespace Blog.Data
{
    public class BlogContext : DbContext
    {
        public BlogContext(DbContextOptions<BlogContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<User>().HasData(new User {Id = 1, Email = "bao@gmail.com", Password = "123"});
            modelBuilder.Entity<User>().HasData(new User {Id = 2, Email = "abc@gmail.com", Password = "123"});

            modelBuilder.Entity<Category>().HasData(new Category {Id = 1, Name = "Category 1"});
            modelBuilder.Entity<Category>().HasData(new Category {Id = 2, Name = "Category 2"});
            modelBuilder.Entity<Category>().HasData(new Category {Id = 3, Name = "Category 3"});
            modelBuilder.Entity<Category>().HasData(new Category {Id = 4, Name = "Category 4"});
        }
    }
}