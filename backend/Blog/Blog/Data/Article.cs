﻿using System;
using System.Collections.Generic;

namespace Blog.Data
{
    public class Article
    {
        public Article()
        {
            DateCreated = DateTime.Now;
            Comments = new HashSet<Comment>();
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public string Picture { get; set; }

        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }
    }
}