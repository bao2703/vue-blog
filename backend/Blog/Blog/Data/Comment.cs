﻿using System;

namespace Blog.Data
{
    public class Comment
    {
        public Comment()
        {
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }

        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public User User { get; set; }

        public int ArticleId { get; set; }

        public Article Article { get; set; }
    }
}