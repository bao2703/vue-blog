﻿using System.Linq;
using Blog.Common;
using Blog.Data;
using Blog.Dtos;
using Blog.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;

        public AccountController(IUnitOfWork unitOfWork, IUserRepository userRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
        }

        [Authorize]
        [HttpGet("articles")]
        public IActionResult GetUserArticles()
        {
            var userId = User.FindFirst("id")?.Value;

            var articles = _userRepository.GetUserArticles(int.Parse(userId)).OrderByDescending(a => a.DateCreated); ;

            return Ok(articles);
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] UserDto dto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = _userRepository.Authenticate(dto.Email, dto.Password);

            if (user == null) return BadRequest(ErrorResponse.InvalidLogin);

            var token = _userRepository.GenerateJwt(user);

            return Ok(new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                Token = token
            });
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody] UserDto dto)
        {
            if (!ModelState.IsValid) return BadRequest();

            if (!_userRepository.IsAvailableEmail(dto.Email))
                return BadRequest(ErrorResponse.NotAvailableEmail);

            var user = new User
            {
                Email = dto.Email,
                Password = dto.Password
            };

            _userRepository.Add(user);
            _unitOfWork.SaveChanges();

            return Ok();
        }
    }
}