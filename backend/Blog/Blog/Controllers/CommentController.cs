﻿using Blog.Data;
using Blog.Dtos;
using Blog.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommentController : Controller
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;

        public CommentController(IUnitOfWork unitOfWork, ICommentRepository commentRepository,
            IUserRepository userRepository)
        {
            _unitOfWork = unitOfWork;
            _commentRepository = commentRepository;
            _userRepository = userRepository;
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create([FromBody] CommentDto dto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var comment = new Comment
            {
                Content = dto.Content,
                ArticleId = dto.ArticleId,
                User = _userRepository.Find(dto.UserId)
            };

            _commentRepository.Add(comment);
            _unitOfWork.SaveChanges();

            return Ok(comment);
        }
    }
}