﻿using System;
using System.IO;
using System.Linq;
using Blog.Common;
using Blog.Data;
using Blog.Dtos;
using Blog.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;

namespace Blog.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IHostingEnvironment _environment;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;

        public ArticleController(
            IUnitOfWork unitOfWork,
            IArticleRepository articleRepository,
            IUserRepository userRepository,
            IHostingEnvironment environment)
        {
            _unitOfWork = unitOfWork;
            _articleRepository = articleRepository;
            _userRepository = userRepository;
            _environment = environment;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var articles = _articleRepository.Include(a => a.User)
                .OrderByDescending(a => a.DateCreated);

            return Ok(articles);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var article = _articleRepository.Include(a => a.User, a => a.Comments)
                .SingleOrDefault(a => a.Id == id);

            if (article == null) return BadRequest();

            article.Comments = article.Comments.OrderByDescending(c => c.DateCreated).ToList();

            return Ok(article);
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create([FromBody] ArticleDto dto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var article = new Article
            {
                Title = dto.Title,
                Content = dto.Content,
                Picture = dto.Picture,
                CategoryId = dto.CategoryId,
                User = _userRepository.Find(dto.UserId)
            };

            _articleRepository.Add(article);
            _unitOfWork.SaveChanges();

            return Ok(article);
        }

        [Authorize]
        [HttpPut]
        public IActionResult Edit([FromBody] ArticleDto dto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var article = _articleRepository.Find(dto.Id);

            if (article == null) return BadRequest();

            article.Title = dto.Title;
            article.Content = dto.Content;
            article.CategoryId = dto.CategoryId;

            if (!string.IsNullOrEmpty(dto.Picture))
            {
                var pathToDelete = Path.Combine(_environment.WebRootPath, article.Picture);
                DeletePicture(pathToDelete);
                article.Picture = dto.Picture;
            }

            _articleRepository.Update(article);
            _unitOfWork.SaveChanges();

            return Ok(article);
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var article = _articleRepository.Find(id);

            if (article == null) return BadRequest();

            var userId = User.FindFirst("id")?.Value;

            if (!IsOwn(article, int.Parse(userId))) return BadRequest();

            var pathToDelete = Path.Combine(_environment.WebRootPath, article.Picture);
            DeletePicture(pathToDelete);

            _articleRepository.Remove(article);
            _unitOfWork.SaveChanges();

            return NoContent();
        }

        [Authorize]
        [HttpPost("upload")]
        public IActionResult Upload(IFormFile file)
        {
            if (file == null) return BadRequest();

            if (!ImageHelper.IsValid(file)) return BadRequest();

            var filePath = UploadImage(file);

            return Ok(filePath);
        }

        private string UploadImage(IFormFile file)
        {
            var filePath = Path.Combine("upload", $"{DateTime.Now.ToFileTime()}{file.FileName}");

            ImageHelper
                .Resize(file.OpenReadStream())
                .Save(Path.Combine(_environment.WebRootPath, filePath));

            return filePath;
        }

        private bool IsOwn(Article article, int userId)
        {
            return article.UserId == userId;
        }

        private void DeletePicture(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }
    }
}