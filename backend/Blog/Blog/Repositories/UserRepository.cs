﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Blog.Common;
using Blog.Data;
using Microsoft.IdentityModel.Tokens;

namespace Blog.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User Authenticate(string email, string password);
        bool IsAvailableEmail(string email);
        string GenerateJwt(User user);
        IEnumerable<Article> GetUserArticles(int userId);
    }

    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(BlogContext context) : base(context)
        {
        }

        public IEnumerable<Article> GetUserArticles(int userId)
        {
            return Include(u => u.Articles).SingleOrDefault(u => u.Id == userId)?.Articles;
        }

        public User Authenticate(string email, string password)
        {
            return SingleOrDefault(u => u.Email == email && u.Password == password);
        }

        public bool IsAvailableEmail(string email)
        {
            return SingleOrDefault(u => u.Email == email) == null;
        }

        public string GenerateJwt(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("id", user.Id.ToString()),
                    new Claim("email", user.Email)
                }),
                Expires = JwtSettings.Expires,
                SigningCredentials = JwtSettings.SigningCredentials
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}