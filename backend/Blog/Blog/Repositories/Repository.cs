﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Blog.Data;
using Microsoft.EntityFrameworkCore;

namespace Blog.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(object id);
        void Remove(TEntity entity);
        TEntity Find(object key);
        IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> Include(params string[] includes);
        IEnumerable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeExpressions);
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> GetAll();
        List<TEntity> ToList();
    }

    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly BlogContext Context;
        protected readonly DbSet<TEntity> DbSet;

        public Repository(BlogContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public TEntity Find(object key)
        {
            return DbSet.Find(key);
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }

        public void Remove(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public void Remove(object id)
        {
            var entity = DbSet.Find(id);
            DbSet.Remove(entity);
        }

        public IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public IEnumerable<TEntity> Include(params string[] includes)
        {
            IQueryable<TEntity> query = DbSet;
            foreach (var include in includes) query = query.Include(include);
            return query ?? DbSet;
        }

        public IEnumerable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            IQueryable<TEntity> query = DbSet;
            foreach (var includeExpression in includeExpressions) query = query.Include(includeExpression);
            return query ?? DbSet;
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.SingleOrDefault(predicate);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.AsEnumerable();
        }

        public List<TEntity> ToList()
        {
            return DbSet.ToList();
        }
    }
}