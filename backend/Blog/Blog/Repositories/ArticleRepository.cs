﻿using Blog.Data;

namespace Blog.Repositories
{
    public interface IArticleRepository : IRepository<Article>
    {
    }

    public class ArticleRepository : Repository<Article>, IArticleRepository
    {
        public ArticleRepository(BlogContext context) : base(context)
        {
        }
    }
}