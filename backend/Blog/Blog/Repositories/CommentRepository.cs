﻿using Blog.Data;

namespace Blog.Repositories
{
    public interface ICommentRepository : IRepository<Comment>
    {
    }

    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(BlogContext context) : base(context)
        {
        }
    }
}